package com.ciuc.andrii.anodatestinstagram.model.entities

import com.google.gson.annotations.SerializedName

data class PhotoData(
    @SerializedName("photo_info") val photoInfo: PhotoInfo,
    @SerializedName("user") val user: User
)