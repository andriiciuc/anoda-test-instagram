package com.ciuc.andrii.anodatestinstagram.model.utils

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import androidx.core.text.toSpanned
import com.ciuc.andrii.anodatestinstagram.model.entities.Comment
import com.ciuc.andrii.anodatestinstagram.model.entities.Like

object LikeParser {
    fun parse(arrayLikes: List<Like>?): Spanned {
        val spannableStringBuilder = SpannableStringBuilder()
        arrayLikes?.let {
            spannableStringBuilder.append("Liked by ")
            if (arrayLikes.isNullOrEmpty().not()) {
                for (i in 0 until 3) {
                    spannableStringBuilder.append(arrayLikes.get(i).userName + if (i == 2) " " else ", ")
                }
                spannableStringBuilder.setSpan(
                    ForegroundColorSpan(Color.BLACK),
                    "Liked by ".length, spannableStringBuilder.length,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )
                if (arrayLikes.size > 3) {
                    spannableStringBuilder.append("and ")
                    val othersString = "${arrayLikes.size - 3} others"
                    spannableStringBuilder.append(othersString)
                    spannableStringBuilder.setSpan(
                        ForegroundColorSpan(Color.BLACK),
                        spannableStringBuilder.length - othersString.length,
                        spannableStringBuilder.length,
                        Spannable.SPAN_INCLUSIVE_INCLUSIVE
                    )
                } else if (arrayLikes.size < 0) {
                    spannableStringBuilder.clear()
                }
            }
        }
        return spannableStringBuilder.toSpanned()
    }
}