package com.ciuc.andrii.anodatestinstagram.model.entities

import com.google.gson.annotations.SerializedName

data class PhotoInfo(
    @SerializedName("comments") val comments: ArrayList<Comment>,
    @SerializedName("image_list") val imageList: ArrayList<String>,
    @SerializedName("likes")val likes: ArrayList<Like>,
    @SerializedName("location")val location: String,
    @SerializedName("published_date")val publishedDate: Long?
)