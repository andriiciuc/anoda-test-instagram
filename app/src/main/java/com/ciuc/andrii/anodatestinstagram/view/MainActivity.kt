package com.ciuc.andrii.anodatestinstagram.view

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ciuc.andrii.anodatestinstagram.R
import com.ciuc.andrii.anodatestinstagram.model.entities.PhotoData
import com.ciuc.andrii.anodatestinstagram.model.utils.*
import com.ciuc.andrii.anodatestinstagram.view.adapter.ChordPagerAdapter
import com.google.android.material.badge.BadgeDrawable.BOTTOM_END
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {

    private var currentPhotoData: PhotoData? = null
    private var imageLikedByUser = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()

        readDataFile()
        setupUI()
    }

    private fun readDataFile() {
        val stringDataRepresentation = DataFileParser.getData(this@MainActivity) ?: ""
        try {
            if (stringDataRepresentation.isNotEmpty()) {
                val mJson = JsonParser.parseString(stringDataRepresentation)
                currentPhotoData = parseJson(mJson)
            }
        } catch (e: java.lang.Exception) {
            toast("Something were wrong")
        }
    }

    private fun parseJson(obj: JsonElement): PhotoData? {
        return try {
            Gson().fromJson(obj, PhotoData::class.java)
        } catch (e: Exception) {
            null
        }
    }

    private fun setupUI() {
        //todo Bottom navigation settings
        bottomNavigation.itemIconTintList = null
        val budgeMain = bottomNavigation.getOrCreateBadge(R.id.menu_main).apply {
            badgeGravity = BOTTOM_END
            backgroundColor = resources.getColor(R.color.instagramBudge)
        }

        imageLikedByUser =
            currentPhotoData?.photoInfo?.likes?.firstOrNull { it.userName == currentPhotoData?.user?.name } != null
        changeLikeImage(imageLikedByUser)

        //todo Set On Click Listeners
        imageLogo.setOnClickListener {
            toast("Image Instagram Logo clicked")
        }

        imageChats.setOnClickListener {
            toast("Image Chats clicked")
        }

        imageUserSmallPhoto.setOnClickListener {
            toast("Image User Photo clicked")
        }

        imageMenu.setOnClickListener {
            toast("Dots Menu clicked")
        }
        imageLike.setOnClickListener {
            toast("Image Like clicked")
            imageLikedByUser = !imageLikedByUser
            changeLikeImage(imageLikedByUser)
        }

        imageAddComment.setOnClickListener {
            toast("Image Add Comment clicked")
        }

        imageGoToChat.setOnClickListener {
            toast("Image Go to chat clicked")
        }

        imageNotes.setOnClickListener {
            toast("Image Notes clicked")
        }

        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_home -> {
                    toast("Menu Home clicked")
                }
                R.id.menu_search -> {
                    toast("Menu Search clicked")
                }
                R.id.menu_add_photo -> {
                    toast("Menu Add Photo clicked")
                }
                R.id.menu_heart -> {
                    toast("Menu Heart clicked")
                }
                R.id.menu_main -> {
                    if (budgeMain != null) budgeMain.isVisible = false
                    toast("Menu Main clicked")
                }
                else -> {
                }
            }
            true
        }

        //todo Photo ViewPager settings
        viewPagerPhoto?.adapter = currentPhotoData?.photoInfo?.imageList?.let {
            ChordPagerAdapter(
                it, this
            )
        }

        if (viewPagerPhoto != null && tabLayoutPhotos != null) {
            TabLayoutMediator(tabLayoutPhotos, viewPagerPhoto)
            { _, _ -> }.attach()
        }

        //todo Setting data to labels
        textUserName.text = currentPhotoData?.user?.name.toString()
        textPhotoLocation.text = currentPhotoData?.photoInfo?.location.toString()

        val timeDifference =
            System.currentTimeMillis() - currentPhotoData?.photoInfo?.publishedDate as Long
        textTimePosted.text = TimeFormatter.format(timeDifference)

        CoroutineScope(IO).launch {
            val drawable = drawableFromUrl(currentPhotoData?.user?.photoUrl)
            withContext(Main) {
                bottomNavigation.menu.findItem(R.id.menu_main).icon = drawable
            }
        }

        Glide.with(this@MainActivity)
            .load(currentPhotoData?.user?.photoUrl)
            .apply(
                RequestOptions().circleCrop()
            )
            .into(imageUserSmallPhoto)


        textLikes.text = LikeParser.parse(currentPhotoData?.photoInfo?.likes)


        val array = currentPhotoData?.photoInfo?.comments ?: arrayListOf()

        val lp = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        if (array.isNotEmpty()) {
            textComments.text = array[0].comment
            for (i in 1 until array.size) {
                val newTextView = TextView(this@MainActivity)


                newTextView.text = CommentParser.parse(array[i].comment, array[i].userName)
                newTextView.layoutParams = lp
                linearSeeMoreComments.addView(newTextView)
            }
        } else {
            textComments.visibility = View.GONE
            textSeeMoreComments.visibility = View.GONE
            linearSeeMoreComments.visibility = View.GONE
        }

        textSeeMoreComments.setOnClickListener {
            linearSeeMoreComments.visibility =
                if (linearSeeMoreComments.visibility == View.GONE) View.VISIBLE else View.GONE
            textSeeMoreComments.text = if (textSeeMoreComments.text == resources.getString(R.string.see_more_comments)) resources.getString(R.string.see_less_comments) else resources.getString(R.string.see_more_comments)
        }
    }

    private fun changeLikeImage(isLiked: Boolean) {
        imageLike.setImageDrawable(
            if (isLiked) resources.getDrawable(R.drawable.ic_heart) else resources.getDrawable(
                R.drawable.ic_heart_menu
            )
        )
    }


    fun drawableFromUrl(url: String?): Drawable? {
        val x: Bitmap
        val connection: HttpURLConnection = URL(url).openConnection() as HttpURLConnection
        connection.connect()
        val input: InputStream = connection.inputStream
        x = BitmapFactory.decodeStream(input)
        return RoundedBitmapDrawableFactory.create(resources, x).apply {
            cornerRadius = 50.0f
            setAntiAlias(true)
        }
    }
}