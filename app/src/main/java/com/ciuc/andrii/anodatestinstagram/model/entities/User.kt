package com.ciuc.andrii.anodatestinstagram.model.entities

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("name") val name: String,
    @SerializedName("photo_url") val photoUrl: String
)