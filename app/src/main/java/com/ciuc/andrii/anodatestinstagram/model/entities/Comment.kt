package com.ciuc.andrii.anodatestinstagram.model.entities

import com.google.gson.annotations.SerializedName

data class Comment(
    @SerializedName("comment") val comment: String,
    @SerializedName("user_id") val userId: Int,
    @SerializedName("user_name") val userName: String
)