package com.ciuc.andrii.anodatestinstagram.view.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ciuc.andrii.anodatestinstagram.R

class ChordPagerAdapter(private var photos: List<String>, val context: Context) :
    RecyclerView.Adapter<PagerVH>() {

    fun fillList(newList: List<String>) {
        photos = listOf<String>()
        photos = newList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH =
        PagerVH(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_photo, parent, false)
        )

    override fun getItemCount(): Int = photos.size

    override fun onBindViewHolder(holder: PagerVH, position: Int) {
        Glide.with(context)
            .load(photos[position])
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(ColorDrawable(Color.WHITE))
            )

            .into(holder.imageUser)
        holder.imageUser.setImageResource(R.drawable.empty_placeholder)
    }
}

class PagerVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val imageUser: ImageView = itemView.findViewById(R.id.imageUser)

}