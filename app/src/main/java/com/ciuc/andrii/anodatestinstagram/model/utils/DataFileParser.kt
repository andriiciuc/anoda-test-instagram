package com.ciuc.andrii.anodatestinstagram.model.utils

import android.content.Context
import android.os.Environment
import android.util.Log
import java.io.File
import java.io.FileInputStream

object DataFileParser {
    private const val fileName = "photoData.json"

    fun getData(context: Context): String? {
        return try {
            val `is` = context.resources.assets.open(fileName)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer)
        } catch (e: Exception) {
            null
        }
    }
}