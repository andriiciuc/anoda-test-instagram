package com.ciuc.andrii.anodatestinstagram.model.utils

import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.MILLISECONDS

object TimeFormatter {
    private const val millisecondsInSecond = 1000
    private const val secondsInMinute = 60
    private const val minutesInHours = 60
    private const val hoursInDays = 24
    private const val dayInMouth = 30

    fun format(
        timeInMilliseconds: Long,
        timeMeasure: TimeMeasure = TimeMeasure.MILLISECONDS
    ): String {
        when (timeMeasure) {
            TimeMeasure.MILLISECONDS -> {
                return if (MILLISECONDS.toSeconds(timeInMilliseconds) > secondsInMinute)
                    format(timeInMilliseconds, TimeMeasure.SECONDS)
                else
                    MILLISECONDS.toSeconds(timeInMilliseconds).toString() + " SECONDS AGO"
            }
            TimeMeasure.SECONDS -> {
                return if (MILLISECONDS.toMinutes(timeInMilliseconds) > minutesInHours)
                    format(timeInMilliseconds, TimeMeasure.MINUTES)
                else
                    MILLISECONDS.toMinutes(timeInMilliseconds).toString() + " MINUTES AGO"
            }
            TimeMeasure.MINUTES -> {
                return if (MILLISECONDS.toHours(timeInMilliseconds) > hoursInDays)
                    format(timeInMilliseconds, TimeMeasure.HOURS)
                else
                    MILLISECONDS.toHours(timeInMilliseconds).toString() + " HOURS AGO"
            }
            TimeMeasure.HOURS -> {
                return if (MILLISECONDS.toDays(timeInMilliseconds) > dayInMouth)
                    format(timeInMilliseconds, TimeMeasure.DAYS)
                else
                    MILLISECONDS.toDays(timeInMilliseconds).toString() + " DAYS AGO"
            }
            TimeMeasure.DAYS -> {
                return MILLISECONDS.toDays(timeInMilliseconds).toString() + " DAYS AGO"
            }
            else -> return MILLISECONDS.toMinutes(timeInMilliseconds).toString() + " MINUTES AGO"
        }
    }
}

enum class TimeMeasure { MILLISECONDS, SECONDS, MINUTES, HOURS, DAYS }