package com.ciuc.andrii.anodatestinstagram.model.utils

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import androidx.core.text.toSpanned

object CommentParser {
    fun parse(comment: String?, author: String): Spanned {
        val spannableStringBuilder = SpannableStringBuilder()
        comment?.let {
            spannableStringBuilder.append("$author ")
            spannableStringBuilder.setSpan(
                ForegroundColorSpan(Color.BLACK),
                0, spannableStringBuilder.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            val spanColor = if (comment.contains('@') || comment.contains('#')) {
                Color.BLUE
            } else Color.BLACK

            spannableStringBuilder.insert(spannableStringBuilder.length, comment)
            spannableStringBuilder.setSpan(
                ForegroundColorSpan(spanColor),
                comment.indexOf(comment), spannableStringBuilder.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        return spannableStringBuilder.toSpanned()
    }
}